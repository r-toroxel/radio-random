import subprocess


def main():
    subprocess.call(['youtube-dl', '-a', '../google-10000-english.txt',
                     '--default-search', 'ytsearch', '--max-filesize',
                     '8m', '-x', 'i'])


if __name__ == "__main__":
    main()
