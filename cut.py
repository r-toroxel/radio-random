import subprocess
import os


def main():
    os.chdir('/home/horace/PycharmProjects/radio/resources/cuts')
    for i in range(0, 35):
        subprocess.call(['ffmpeg', '-ss', str(i*2), '-t',
                         '5', '-i', '../skating.opus',
                         'out.opus'])
        os.rename("out.opus", str("out" + str(i) + ".opus"))


if __name__ == "__main__":
    main()
