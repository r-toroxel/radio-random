import os
import subprocess
from random import shuffle
from pydub import AudioSegment, effects


def main():
    lst = os.listdir('resources/cuts/')
    shuffle(lst)
    os.chdir('resources/cuts')
    sounds = list(map(lambda x: AudioSegment.from_file(x, codec="opus"), lst))
    concat = sounds[0]
    for s in sounds[1:]:
        print(len(s))
        concat += s

    concat = effects.normalize(concat)

    concat.export("../output.opus", format="opus")


if __name__ == "__main__":
    main()
